﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The class to link the GAgent code with the Inspector Editor so the agent's
//properties can be displayed in the Inspector
[ExecuteInEditMode]
public class GAgentVisual : MonoBehaviour
{
    public GAgent thisAgent;


    void Start()
    {
        thisAgent = this.GetComponent<GAgent>();
    }


    void Update()
    {
        
    }
}
