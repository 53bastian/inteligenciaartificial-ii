﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SubGoal {

    // diccionario de los objetivos
    public Dictionary<string, int> sGoals;
    // variable de leminacion despues de cumplir el objetivo
    public bool remove;


    public SubGoal(string s, int i, bool r) {

        sGoals = new Dictionary<string, int>();
        sGoals.Add(s, i);
        remove = r;
    }
}

public class GAgent : MonoBehaviour {

    // lista acciones
    public List<GAction> actions = new List<GAction>();
    // Diccionario de subobjetivos
    public Dictionary<SubGoal, int> goals = new Dictionary<SubGoal, int>();
    // inventario
    public GInventory inventory = new GInventory();
    // credenciales
    public WorldStates beliefs = new WorldStates();

    // acceder a la planificadora
    GPlanner planner;
    // accion de la cola
    Queue<GAction> actionQueue;
    // respuesta de la cola
    public GAction currentAction;
    // subojtivos
    SubGoal currentGoal;


    public void Start() {

        GAction[] acts = this.GetComponents<GAction>();
        foreach (GAction a in acts) {

            actions.Add(a);
        }
    }

    bool invoked = false;
    //accion del agente con su ubicaion
  
    public void CompleteAction() {

        currentAction.running = false;
        currentAction.PostPerform();
        invoked = false;
    }

    void LateUpdate() {

        //accion actual y ejecucion
        if (currentAction != null && currentAction.running) {

            // Find the distance to the target
            float distanceToTarget = Vector3.Distance(currentAction.target.transform.position, this.transform.position);
            // Elagente alcance el objetivo
            if (currentAction.agent.hasPath && distanceToTarget < 2.0f) { // currentAction.agent.remainingDistance < 1.0f) 

                if (!invoked) {

                    //el movieminto de accion esta completo espera un tiempo 

                    Invoke("CompleteAction", currentAction.duration);
                    invoked = true;
                }
            }
            return;
        }

        // planificador y accion de cola
        if (planner == null || actionQueue == null) {

            // planificador esta vaio crea uno
            planner = new GPlanner();

            // orden de los objetivos
            var sortedGoals = from entry in goals orderby entry.Value descending select entry;

            //revision del objeto para cada plan ancanzable 
            foreach (KeyValuePair<SubGoal, int> sg in sortedGoals) {

                actionQueue = planner.plan(actions, sg.Key.sGoals, beliefs);
                // Obtencion del plan
                if (actionQueue != null) {

                    // establece el objetivo actual
                    currentGoal = sg.Key;
                    break;
                }
            }
        }

        // accion de cola
        if (actionQueue != null && actionQueue.Count == 0) {

            // comprueba la respuesta 
            if (currentGoal.remove) {

                // elimina el objetivo
                goals.Remove(currentGoal);
            }
            // el planificador lo activa de nuevo
            planner = null;
        }

        // acciones por cumplir
        if (actionQueue != null && actionQueue.Count > 0) {

            //elminacion de la ccion superior a la cola y obtener una repsuesta
            currentAction = actionQueue.Dequeue();

            if (currentAction.PrePerform()) {

                // obtener el objeto actual
                if (currentAction.target == null && currentAction.targetTag != "") {

                    currentAction.target = GameObject.FindWithTag(currentAction.targetTag);
                }

                if (currentAction.target != null) {

                    // activar la respuesta de la accion
                    currentAction.running = true;
                    // unidades de destino hacia el agente 
                    currentAction.agent.SetDestination(currentAction.target.transform.position);
                }
            } else {

                // forza un nuevo plan
                actionQueue = null;
            }
        }
    }
}
