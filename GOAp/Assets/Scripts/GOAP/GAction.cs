﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class GAction : MonoBehaviour {

    // Nombre de laccion
    public string actionName = "Action";
    // costo de la accion
    public float cost = 1.0f;
    // Target donde se va desarrollar la accion
    public GameObject target;
    // guarda el tag
    public string targetTag;
    // Duracion que debe tomar la acción
    public float duration = 0.0f;
    // Condicione sprevias
    public WorldState[] preConditions;
    // estados de afterEffects
    public WorldState[] afterEffects;
    // el NavMEshAgent junto al agente
    public NavMeshAgent agent;
    // descicionario de condiciones previas
    public Dictionary<string, int> preconditions;
    // dicionario de efectos
    public Dictionary<string, int> effects;
    //estados de lso agentes
    public WorldStates agentBeliefs;
    // accede al invetari(incuvadora)
    public GInventory inventory;
    public WorldStates beliefs;
    // Accion actual
    public bool running = false;


    public GAction() {

        // Set up the preconditions and effects
        preconditions = new Dictionary<string, int>();
        effects = new Dictionary<string, int>();
    }

    private void Awake() {

        // Configurar las condiciones previas y los efectos.
        agent = this.gameObject.GetComponent<NavMeshAgent>();

        // condiciones previas ya agregar al diccionario

        if (preConditions != null) {

            foreach (WorldState w in preConditions) {

                // Agregue cada elemento a nuestro Diccionario
                preconditions.Add(w.key, w.value);
            }
        }

        // condiciones previas ya agregar al diccionario
        if (afterEffects != null) {

            foreach (WorldState w in afterEffects) {

                // Add each item to our Dictionary
                effects.Add(w.key, w.value);
            }
        }
        // Rellena el inventario
        inventory = this.GetComponent<GAgent>().inventory;
        // crwedenciales del agente
        beliefs = this.GetComponent<GAgent>().beliefs;
    }

    public bool IsAchievable() {

        return true;
    }

    //verifica la accion del mundo para coincidir con las acciones

    public bool IsAhievableGiven(Dictionary<string, int> conditions) {

        foreach (KeyValuePair<string, int> p in preconditions) {

            if (!conditions.ContainsKey(p.Key)) {

                return false;
            }
        }
        return true;
    }

    public abstract bool PrePerform();
    public abstract bool PostPerform();
}
