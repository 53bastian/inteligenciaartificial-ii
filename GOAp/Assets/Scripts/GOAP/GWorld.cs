﻿using System.Collections.Generic;
using UnityEngine;

public sealed class GWorld {

    //intancia de la clase script
    private static readonly GWorld instance = new GWorld();
    // Estados del mundo
    private static WorldStates world;
    // Cola de pacientes
    private static Queue<GameObject> patients;
    // Cola de cubiculos
    private static Queue<GameObject> cubicles;

    static GWorld() {

        // Creamos un nuevo mundo
        world = new WorldStates();
        // Arreglo cola  de pacientes
        patients = new Queue<GameObject>();
        // Arreglo de paciente
        cubicles = new Queue<GameObject>();
        // Encontrar con el tag los gamobjects cubiculo
        GameObject[] cubes = GameObject.FindGameObjectsWithTag("Cubicle");
        // Agregar a cola de cubiculos
        foreach (GameObject c in cubes) {

            cubicles.Enqueue(c);
        }

        // Inform the state
        if (cubes.Length > 0) {
            world.ModifyState("FreeCubicle", cubes.Length);
        }

        // Escala del tiempo en ejecucion de unity
        Time.timeScale = 5.0f;
    }

    private GWorld() {

    }

    // agrega pacientes
    public void AddPatient(GameObject p) {

        // Agregar pacientes para cola
        patients.Enqueue(p);
    }

    // Elimianr pacientes
    public GameObject RemovePatient() {

        if (patients.Count == 0) return null;
        return patients.Dequeue();
    }

    //Agrega cubiculos
    public void AddCubicle(GameObject p) {

        // Agregar pacientes a la cola
        cubicles.Enqueue(p);
    }

    // Elimiando cubiculos
    public GameObject RemoveCubicle() {

        // Eliminar un cubiculo exsitente 
        if (cubicles.Count == 0) return null;
        return cubicles.Dequeue();
    }

    public static GWorld Instance {

        get { return instance; }
    }

    public WorldStates GetWorld() {

        return world;
    }
}
