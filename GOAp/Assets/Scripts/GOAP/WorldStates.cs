﻿using System.Collections.Generic;

//make the dictionary elements their own serializable class
//so we can edit them in the inspector
[System.Serializable]
public class WorldState {

    public string key;
    public int value;
}

public class WorldStates {

    // Diccionario
    public Dictionary<string, int> states;

    public WorldStates() {

        states = new Dictionary<string, int>();
    }


    // Funcion para la clave
    public bool HasState(string key) {

        return states.ContainsKey(key);
    }

    // Agregamos a nuestro diccionario
    private void AddState(string key, int value) {

        states.Add(key, value);
    }

    public void ModifyState(string key, int value) {

        // Si contiene la palabra clave
        if (HasState(key)) {

            // Agrega el valor a la clave
            states[key] += value;
            // Si es menor a 0 se lemina
            if (states[key] <= 0) {

                // llama al  metodo elminar clave 
                RemoveState(key);
            }
        } else {

            AddState(key, value);
        }
    }

    //metodo de remover estado
    public void RemoveState(string key) {

        // mira si pirmero existe
        if (HasState(key)) {

            states.Remove(key);
        }
    }

    // Se colcoa en el estado
    public void SetState(string key, int value) {

        // comprueba si existe
        if (HasState(key)) {

            states[key] = value;
        } else {

            AddState(key, value);
        }
    }

    public Dictionary<string, int> GetStates() {

        return states;
    }
}
