﻿using System.Collections.Generic;
using UnityEngine;

public class GInventory {
    // lista de incuvadoras
    public List<GameObject> items = new List<GameObject>();

    // Agrega incuvadoras a la lis
    public void AddItem(GameObject i) {

        items.Add(i);
    }

    // Busqueda del tag en particular
    public GameObject FindItemWithTag(string tag) {

        // interaccion de los elementos
        foreach (GameObject i in items) {

            // Deben coincidir
            if (i.tag == tag) {

                return i;
            }
        }
        // no lo encuentra
        return null;
    }

    //eliminar el item de la lista
    public void RemoveItem(GameObject i) {

        int indexToRemove = -1;

        // Busque en la lista para ver si existe
        foreach (GameObject g in items) {

            // establecer el index al primer elmento de la lis
            indexToRemove++;
            // Se encontro
            if (g == i) {

                break;
            }
        }
        // Si se quita
        if (indexToRemove >= 1) {

            // Se elmimina el la varable 
            items.RemoveAt(indexToRemove);
        }
    }
}
