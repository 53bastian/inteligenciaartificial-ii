using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    public Rigidbody rb;
    public float moveSpeed = 10.0f;
    public float rotationSpeed = 10.0f;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void FixedUpdater()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        if (v != 0)
        {
            //rb.velocity = new Vector3(0, 0, moveSpeed * v*Time.deltaTime);
            //rb.MovePosition(transform.position + (transform.forward*v*Time.deltaTime));
            transform.Translate(0, 0, moveSpeed * v * Time.fixedDeltaTime);
            //rb.AddForce(0, 0, moveSpeed * v * Time.fixedDeltaTime,ForceMode.Impulse);
        }

        if (h!=0)
        {
            transform.Rotate(0, h * 150 * Time.fixedDeltaTime,0);
        }
    }
  

}
