using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PayasoRun : PayasoStateMachine1
{
    public float speed;
    public float distance;
    public float distanceToOrigin;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Vector2 v1;
        Vector2 v2;
        v1 = new Vector2(payaso.origin.x, 0);
        v2 = new Vector2(payaso.transform.position.x, 0);
        distance = Vector2.Distance(GameManager.instance.player.transform.position, payaso.transform.position);
        distanceToOrigin = Vector2.Distance(v1, v2);
       

        if((payaso.distance>=15f) && animator.GetBool("NearPlayer"))
        {
            Debug.Log("2");
            animator.SetBool("FarFromHome",true);
            animator.SetBool("NearPlayer", false);
        }
        if (distanceToOrigin <= 2f && !animator.GetBool("NearPlayer"))
        {
            Debug.Log("3");
            animator.SetBool("FarFromHome", false);
            //payaso.rb.velocity = Vector2.zero;
            return;
        }

        payaso.Move(animator.GetBool("FarFromHome"));
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
