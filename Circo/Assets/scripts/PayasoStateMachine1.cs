using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PayasoStateMachine1 : StateMachineBehaviour
{
    public IAPayaso payaso;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        payaso = animator.gameObject.GetComponent<IAPayaso>();
    }
}
