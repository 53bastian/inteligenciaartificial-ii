using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public int stars = 4;
    public int currentStars = 0;
    public PlayerController player;
    public bool gameInProgress = false;
    public Text dialogueText;
    public GameObject acertijoPanel;
    public GameObject dialoguePanel;
    public GameObject winPanel;
    public List<Star> starsGO;
    public int starsGoIndex=0;
    public static GameManager instance;
    public Text starsText;

    private void Awake()
    {
        if (instance==null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        if (gameInProgress)
        {
            player.FixedUpdater();
        }
    }
    public void UpdateStars()
    {
        currentStars++;
        starsText.text = "Stars " + currentStars + "/" + stars;
        if (currentStars >= stars)
        {
            EndGame(true);
        }
    }

    public void EndGame(bool win)
    {
        if (!win)
        {
            SceneManager.LoadScene("SampleScene");
        }
        else
        {
            winPanel.SetActive(true);
        }
    }
    public void LoadDialogue(string dialogue)
    {
        gameInProgress = false;
        dialogueText.text = dialogue;
        dialoguePanel.SetActive(true);
    }
    public void CloseDialogueManager()
    {
        ShowStar();
        dialoguePanel.SetActive(false);
        acertijoPanel.SetActive(false);
        gameInProgress = true;
    }
    public void ShowStar()
    {
        starsGO[starsGoIndex].gameObject.SetActive(true);
        starsGoIndex++;
    }
    public void LoadAcertijo()
    {
        acertijoPanel.SetActive(true);
        gameInProgress = false;
    }
}
