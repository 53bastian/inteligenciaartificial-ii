using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IaOso : MonoBehaviour
{
    public float timebtwAttack= 1.5f;
    public float timer = 0.0f;
    public float distance = 0.0f;
    public BulletOso bulletOsoPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(GameManager.instance.player.transform.position, transform.position);
        if (timer <timebtwAttack)
        {
            timer += Time.deltaTime;
        }
    }
    public void Attack()
    {
        timer = 0;
        BulletOso b = Instantiate(bulletOsoPrefab,transform.position,transform.rotation);
        b.Init(1);
    }
}
