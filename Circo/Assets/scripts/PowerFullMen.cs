using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerFullMen : MonoBehaviour
{
    public bool showDialogue = true;
    private void OnTriggerEnter(Collider other)
    {
        if (showDialogue)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                GameManager.instance.LoadAcertijo();
            }
            showDialogue = false;
        }

    }
}
