using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class IAPayaso : MonoBehaviour
{
    public NavMeshAgent agent;
    public float distance = 0f;
    [HideInInspector]public Vector3 origin;
    
    void Awake() 
    {
        origin = new Vector2(transform.position.x, transform.position.y);    
    }
    
    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 v1 = new Vector2(GameManager.instance.player.transform.position.x, 0);
        Vector2 v2 = new Vector2(origin.x, 0);
    }


    public void Move(bool farFromHome)
    {
        if (farFromHome)
        {
            agent.SetDestination(origin);
        }
        else
        {
            agent.SetDestination(GameManager.instance.player.transform.position);
            
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            GameManager.instance.EndGame(false);
        }
    }
   



    
    IEnumerator DestroyEnemy() 
    {
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }

}