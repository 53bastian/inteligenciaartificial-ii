using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OsoIdle : OsoStateMachine2
{


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }



    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
            
    {

        if (oso.distance <= 10f && oso.timer >= oso.timebtwAttack)
        {

            animator.SetBool("canAttack", true);
        }

    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
