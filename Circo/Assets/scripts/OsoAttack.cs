using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OsoAttack : OsoStateMachine2
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
        oso.transform.LookAt(new Vector3(GameManager.instance.player.transform.position.x,oso.transform.position.y, GameManager.instance.player.transform.position.z));
        oso.Attack();
        animator.SetBool("canAttack", false);

    }



    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)

    {

    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
