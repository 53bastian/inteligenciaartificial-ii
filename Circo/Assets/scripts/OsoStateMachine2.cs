using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OsoStateMachine2 : StateMachineBehaviour
{
    public IaOso oso;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        oso = animator.gameObject.GetComponent<IaOso>();
    }
}
