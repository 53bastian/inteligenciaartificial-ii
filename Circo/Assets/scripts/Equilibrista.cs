using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equilibrista : MonoBehaviour
{
    
    [TextArea]public string dialogue;
    public bool showDialogue= true;
    private void OnTriggerEnter(Collider other)
    {
        if (showDialogue)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                GameManager.instance.LoadDialogue(dialogue);
            }
            showDialogue = false;
        }
        
    }
}
