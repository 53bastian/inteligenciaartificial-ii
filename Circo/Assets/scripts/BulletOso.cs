using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletOso : MonoBehaviour
{
    public float damage;
    bool canDamage = true;
    public Rigidbody rb;
    public float bulletForce = 2f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Init(float damage)
    {
        this.damage = damage;
        gameObject.SetActive(true);
        rb.AddForce(transform.forward * bulletForce, ForceMode.Impulse);
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (canDamage)
        {
            Debug.Log(collision.gameObject.name);
            PlayerController p = collision.GetComponent<PlayerController>();
            if (p != null)
            {
                canDamage = false;
                GameManager.instance.EndGame(false);
            }
            Destroyer();
        }
    }


    void Destroyer()
    {
        Destroy(gameObject);
    }
}
