using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PayasoIdle : PayasoStateMachine1
{


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);
    }



    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
            
    {


        float distance = Vector3.Distance(GameManager.instance.player.transform.position, payaso.transform.position);
        if (distance <= 5f && payaso.distance <= 15f)
        {

            animator.SetBool("NearPlayer", true);
        }
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
